import psycopg2
import random
import time

conn = psycopg2.connect(
    host="localhost",
    database="postgres",
    user="postgres",
    password="root")

cursor = conn.cursor()

create_table = \
    """
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
  "first_name" varchar,
  "last_name" varchar,
  "age" int8,
  "job" varchar,
  PRIMARY KEY ("id")
);
"""
insert_query = \
    """
  INSERT INTO "public"."users"("first_name", "last_name", "age", "job") VALUES (%s, %s, %s, %s)
"""

try:
    cursor.execute(create_table)
    conn.commit()
except:
    print('Table "users" already exists')

cursor.execute("ROLLBACK")

first_name_list = ['ali', 'amir', 'reza', 'zahra', 'fateme', 'narges']
last_name_list = ['marefati', 'maleki',
                  'rezae', 'vahedi', 'ghorbani', 'mohammadi']
job_list = ['back-end', 'front-end', 'devop', 'sysadmin', 'sre']

i = 0
while i <= 100:
    t1 = time.time()
    first_name = random.choice(first_name_list)
    last_name = random.choice(last_name_list)
    job = random.choice(job_list)
    age = random.randint(18, 30)
    cursor.execute(insert_query, (first_name, last_name, age, job))
    conn.commit()
    t2 = time.time()
    print(f"{i} insert data in {(str(t2-t1)[:5])} s")
    i += 1
    time.sleep(1/1000)

cursor.close()
conn.close()
